import pandas as pd
import plotly.express as px
from sklearn.cluster import KMeans, AgglomerativeClustering

def cluster_and_visualize(data, n_clusters=2, method='kmeans'):
    """
    Applique le clustering sur les données et retourne une carte interactive des résultats.

    Parameters:
    data (pd.DataFrame): Le dataframe contenant les données avec les colonnes 'latitude', 'longitude', et 'haut_tot'.
    n_clusters (int): Le nombre de clusters à créer.
    method (str): La méthode de clustering à utiliser ('kmeans' ou 'agglomerative').

    Returns:
    fig (plotly.graph_objs._figure.Figure): La figure Plotly de la carte interactive.
    """
    if method == 'kmeans' or method == 'k':
        model = KMeans(n_clusters=n_clusters, random_state=0)
    elif method == 'agglomerative' or method == 'a':
        model = AgglomerativeClustering(n_clusters=n_clusters)
    else:
        raise ValueError("Méthode de clustering non reconnue. Utilisez 'kmeans' ou 'agglomerative'.")

    # Appliquer le clustering
    data['cluster'] = model.fit_predict(data[['haut_tot']])

    # Créer la visualisation sur la carte
    fig = px.scatter_mapbox(data,
                            lat="latitude",
                            lon="longitude",
                            color="cluster",
                            color_discrete_sequence=px.colors.qualitative.Plotly,
                            size_max=15,
                            zoom=10,
                            mapbox_style="carto-positron")
    return fig

def main():
    # Charger les données

    df = pd.read_csv("./Data_Arbre.csv")  # Remplacez par le chemin vers votre fichier de données
    df = df[['latitude', 'longitude', 'haut_tot']].dropna()

    # Demander à l'utilisateur de choisir le nombre de clusters et la méthode de clustering
    n_clusters = int(input("Entrez le nombre de clusters : "))
    method = input("Entrez la méthode de clustering ('[K]MEANS' ou '[a]gglomerative') : ").strip().lower()

    # Générer la carte
    fig = cluster_and_visualize(df, n_clusters, method)

    # Sauvegarder et afficher la carte
    file_name = f"{method}_clustering_map.html"
    fig.write_html(file_name)
    fig.show()
    print(f"La carte a été sauvegardée dans '{file_name}'. Ouvrez ce fichier dans votre navigateur pour voir la carte.")

if __name__ == "__main__":
    print(2)
    main()
