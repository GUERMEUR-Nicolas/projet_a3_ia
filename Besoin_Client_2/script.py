import pandas as pd
import joblib
import json
import os

def load_model_and_scalers(filepath):
    model = joblib.load(filepath + '/best_Random_Forest.pkl')
    scaler_X = joblib.load(filepath + '/scaler_X.pkl')
    scaler_y = joblib.load(filepath + '/scaler_y.pkl')
    return model, scaler_X, scaler_y

def load_data(json_path):
    with open(json_path, 'r') as file:
        data = json.load(file)
    df = pd.DataFrame(data)
    return df

def save_predictions(predictions, output_path):
    with open(output_path, 'w') as json_file:
        json_file.write(predictions)
    print(f"Predictions saved to {output_path}")

def predict_age(json_input, filepath):
    # Charger les données de test depuis le JSON
#    print(json_input)
    data_test = pd.read_json(json_input)

    # Charger le modèle et les scalers sauvegardés
    model, scaler_X, scaler_y = load_model_and_scalers(filepath)

    # Normaliser les données d'entrée
    X_test_scaled = scaler_X.transform(data_test)

    # Faire des prédictions
    y_pred_scaled = model.predict(X_test_scaled)

    # Inverser la normalisation des prédictions
    y_pred = scaler_y.inverse_transform(y_pred_scaled.reshape(-1, 1))

    # Convertir les résultats en JSON
    predictions = pd.DataFrame(y_pred, columns=['age_predicted'])
    result_json = predictions.to_json(orient="records", indent=4)
    return result_json

if __name__ == "__main__":
    script_dir = os.path.dirname(os.path.abspath(__file__))
    json_path = os.path.join(script_dir, 'DataFrame_test.json')
#    json_input = load_data(json_path)
    json_input = '''[
        {"haut_tot": 10.0, "tronc_diam": 30.0, "fk_prec_estim": 5},
        {"haut_tot": 15.0, "tronc_diam": 35.0, "fk_prec_estim": 7}
    ]'''
    result_json = predict_age(json_input, script_dir)
    save_predictions(result_json, os.path.join(script_dir, 'predicted_ages.json'))
