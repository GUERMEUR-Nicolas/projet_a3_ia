1) Il faut être dans le même dossier que le script
2) Il faut lancer le script avec python (e.g `python3 script.py`)
3) Si le script demande de choisir un modèle, la lettre entres crochets [] peut
	être utilisée seule pour sélectionner ce modèle.
