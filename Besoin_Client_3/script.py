import pandas as pd
import folium
from sklearn.preprocessing import StandardScaler, LabelEncoder
import joblib

def afficher_carte_arbre_deracine(data_csv):

    method = input("Entrez la méthode de clustering ('[r]andom_forest' ou '[b]oosting ou  b[a]gging') : ").strip().lower()
    
    if method == 'random_forest' or method == 'r':
        loaded_model = joblib.load('random_forest_regressor.pkl')
    elif method == 'boosting' or method == 'b':
        loaded_model = joblib.load('gradient_boosting_regressor.pkl')
    elif method == 'bagging' or method == 'a':
        loaded_model = joblib.load('bagging_regressor.pkl')
    else:
        raise ValueError("Méthode de clustering non reconnue. Utilisez 'random_forest' ou 'boosting ou bagging'.")




    # Charger les données depuis un fichier CSV
    data_test = pd.read_csv(data_csv)
    loaded_scaler = joblib.load('scaler.pkl')
    loaded_label_encoder_arb_etat = joblib.load('label_encoder_fk_arb_etat.pkl')
    categorical_columns = ['fk_prec_estim', 'fk_stadedev']
    loaded_label_encoders = {column: joblib.load(f'label_encoder_{column}.pkl') for column in categorical_columns}

    # Sélectionner les colonnes pertinentes
    columns_of_interest = ['haut_tot', 'tronc_diam', 'age_estim', 'fk_prec_estim', 'clc_nbr_diag', 'fk_stadedev', 'latitude', 'longitude']
    data_test = data_test[columns_of_interest].copy()
    
    # Encodage des colonnes catégorielles avec gestion des valeurs inconnues
    for column in categorical_columns:
        le = loaded_label_encoders[column]
        data_test[column] = data_test[column].apply(lambda x: le.transform([x])[0] if x in le.classes_ else -1)

    # Normalisation des données
    features = ['haut_tot', 'tronc_diam', 'age_estim', 'fk_prec_estim', 'clc_nbr_diag', 'fk_stadedev']
    X_new = data_test[features]
    
    # Vérifier que les colonnes correspondent exactement
    expected_feature_names = loaded_scaler.feature_names_in_
    X_new = X_new[expected_feature_names]

    X_new_scaled = loaded_scaler.transform(X_new)
    
    # Prédiction
    y_new_pred = loaded_model.predict(X_new_scaled)
    
    # Ajouter les prédictions au DataFrame
    data_test['déraciner'] = (y_new_pred >= 0.5).astype(int)
    
    # Filtrer les arbres déracinés
    deraciner_list = data_test[data_test['déraciner'] == 1]
    
    # Créer une carte avec folium
    carte = folium.Map(location=[data_test['latitude'].mean(), data_test['longitude'].mean()], zoom_start=13)
    
    # Ajouter les arbres déracinés à la carte
    for idx, row in deraciner_list.iterrows():
        folium.Marker([row['latitude'], row['longitude']], 
                      popup=f"Arbre déraciné: {row['déraciner']}").add_to(carte)
    
    # Afficher le nombre total d'arbres déracinés
    nombre_arbre_deraciner = deraciner_list.shape[0]
    print(f"Nombre total d'arbres déracinés : {nombre_arbre_deraciner}")
    
    # Sauvegarder la carte en tant que fichier HTML
    carte.save('carte_arbre_deracine.html')
    print("La carte des arbres déracinés a été créée avec succès.")
    
    return carte

# Exemple d'utilisation de la fonction
# Suppose que le fichier CSV s'appelle 'Data_Arbre_Modified.csv'
afficher_carte_arbre_deracine('Data_Arbre_Modified.csv')
